Role Name
=========

httpd-service role which installs httpd from rpm package

basic auth login and password for apache welcome page are: welcome eeMoo9kohJ0quo

vault password is: vltpass888

Requirements
------------

CentOS 7.5+
Ansible 2.6+

Role installs packages apr, apr-util, httpd-tools, mailcap from yum repository automatically.
Strategy must be set to linear (one by one) in order to deploy without affecting customer experience 
on cluster with 3 nodes or more.

Role Variables
--------------

```httpd_rpm_url: "http://mirror.centos.org/centos/7/os/x86_64/Packages/httpd-2.4.6-93.el7.centos.x86_64.rpm"```

Variable with rpm path must be set explicitly 

```rollback: [yes|no]```

Allow rollback for version downgrade. Setting this parameter to no has the side effect of making
the yum module behave in a non-idempotent way, according to the module documentation.


```reinstall: [true|false]```

Allow reinstall. Though a way to forcibly uninstalls and reinstalls something that looks like the same
package in the same task goes against the philosophy of Ansible, which is that it looks at the state of a system,
and only makes changes where the state differs from the one a playbook defines.


Dependencies
------------

none

Example Playbook
----------------

```yaml
- name: Install and configure httpd
  hosts: httpd_hosts
  any_errors_fatal: true
  strategy: linear
  become: yes
  roles:
      - httpd-service
```

Where httpd_hosts is hosts group of inventory which contains of multiple servers.

License
-------

BSD

Author Information
------------------

Anton Akulov for Textkernel
